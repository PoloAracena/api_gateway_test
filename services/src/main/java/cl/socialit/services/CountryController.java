package cl.socialit.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import cl.socialit.services.rest.templates.CountryDaily;

@RestController
public class CountryController {
	@Value("${urls.externalCountry}")
	private String externalUrl;
	
	@CrossOrigin
	@RequestMapping("/country/{countryName}")
	public ResponseEntity<CountryDaily[]> country(RestTemplate restTemplate, @PathVariable String countryName) {
			ResponseEntity<CountryDaily[]> dailyData = restTemplate.getForEntity(externalUrl+"/"+countryName, CountryDaily[].class);
			ResponseEntity<CountryDaily[]> newResponse = new ResponseEntity<CountryDaily[]>(dailyData.getBody(), HttpStatus.OK);
			return newResponse;
	}
}
