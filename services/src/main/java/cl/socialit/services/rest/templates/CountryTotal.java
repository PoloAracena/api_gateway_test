package cl.socialit.services.rest.templates;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CountryTotal {
	@JsonProperty("Country")
	private String Country;
    @JsonProperty("TotalConfirmed")
	private int TotalConfirmed;
    @JsonProperty("TotalDeaths")
	private int TotalDeaths;
    @JsonProperty("TotalRecovered")
	private int TotalRecovered;    
    @JsonProperty("Slug")
	private String Slug;
}
