package cl.socialit.services.rest.templates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CountryDaily {
	@JsonProperty("Active")
	private int Active;
	@JsonProperty("Confirmed")
	private int Confirmed;
	@JsonProperty("Deaths")
	private int Deaths;
	@JsonProperty("Recovered")
	private int Recovered;
	@JsonProperty("Date")
	private String Date;
	
	public CountryDaily() {
		
	}
}
