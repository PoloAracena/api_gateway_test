package cl.socialit.services.rest.templates;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Summary {

	@JsonProperty("ID")
	private String ID;
	@JsonProperty("Message")
	private String Message;
	@JsonProperty("Global")
	private Global Global;
	@JsonProperty("Countries")
	private CountryTotal[] Countries;
	@JsonProperty("Date")
	private String Date;
	
	
	public Summary() {
		
	}
	
	public int get_countries_amount() {
		return getCountries().length;
	}

	public CountryTotal[] getCountries() {
		return Countries;
	}
}
