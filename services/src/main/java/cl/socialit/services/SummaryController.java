package cl.socialit.services;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import cl.socialit.services.rest.templates.CountryTotal;
import cl.socialit.services.rest.templates.Summary;

@RestController
public class SummaryController {
	@Value("${urls.externalSummary}")
	private String externalUrl;

	@CrossOrigin
	@RequestMapping("/summary")
	public ResponseEntity<CountryTotal[]> summary(RestTemplate restTemplate) {
		ResponseEntity<Summary> summaryData = restTemplate.getForEntity(externalUrl, Summary.class);
		CountryTotal[] countriesTotal = Arrays.copyOf(summaryData.getBody().getCountries(), summaryData.getBody().get_countries_amount());
		ResponseEntity<CountryTotal[]> countryTotals = new ResponseEntity<CountryTotal[]>(countriesTotal, HttpStatus.OK);
		return countryTotals;
	}
}
