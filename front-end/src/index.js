import React from 'react';
import ReactDOM from 'react-dom';
import registerServiceWorker from './registerServiceWorker';

import App from './App';

import store from './redux/store/pais/pais';

ReactDOM.render(<App store={store} />, document.getElementById('root'));
registerServiceWorker();
