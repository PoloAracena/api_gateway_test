import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Provider } from 'react-redux';
import PropTypes from 'prop-types';

import TablaCovid from './pages/Home';
import TablaCiudad from './components/historico'

const App = ({ store }) => (
	<Provider store={store}>
		<Router>
			<Switch>
					<Route exact path="/">
						<TablaCovid></TablaCovid>
					</Route>
					<Route exact path="/historico">
						<TablaCiudad></TablaCiudad>
					</Route>
			</Switch>
		</Router>
	</Provider>
);

App.propTypes = {
	store: PropTypes.object.isRequired
};

export default App;
