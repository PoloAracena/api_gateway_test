// Las acciones siempre tienen la estructura { type, payload }
export const modificaPais = (pais) => {
    return {
        type: 'MODIFY_COUNTRY',
        payload: pais
    }
};
