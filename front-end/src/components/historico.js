import React, { useState, useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import MaterialTable from 'material-table';
import axios from 'axios';
import { Button } from '@material-ui/core'
import { connect } from 'react-redux';
import { selectActiveWord } from '../redux/reducers/pais/pais';


const columnas = [
	{ title: 'Casos activos', field: 'Active' },
	{ title: 'Casos confirmados', field: 'Confirmed' },
	{ title: 'Muertes', field: 'Deaths' },
    { title: 'Recuperaciones', field: 'Recovered' },
    { title: 'Fecha', field: 'Date' },
]

const baseUrl = 'https://api-gateway-test-2101.herokuapp.com/country/';

const TablaCiudad = ({pais}) => {
    let history = useHistory();

  	const redirect = () => {
    	history.push('/')
	  }

	const [data, setData] = useState([]);

	const peticionGet = async () => {
		await axios.get(baseUrl+pais)
			.then(response => {
				setData(response.data)
			})
	}

	useEffect(() => {
		peticionGet();
	}, [])
    let tituloTable = 'Listado de Casos Historicos ' + pais
	return (<div>
        <Button onClick={() =>  {redirect()} }>Volver</Button>
		<MaterialTable
			title={tituloTable}
			columns={columnas}
			data={data}
		/>
	</div>)
}

function mapStateToProps(state) {
    return {
        pais: selectActiveWord(state)
    }
}

export default connect(mapStateToProps)(TablaCiudad);



