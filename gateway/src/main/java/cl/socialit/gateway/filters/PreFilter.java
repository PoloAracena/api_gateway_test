package cl.socialit.gateway.filters;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.exception.ZuulException;

public class PreFilter extends ZuulFilter {
    private static Logger log = LoggerFactory.getLogger(PreFilter.class);


	public boolean shouldFilter() {
		// TODO Auto-generated method stub
		return true;
	}

	public Object run() throws ZuulException {
		return null;
	}

	@Override
	public String filterType() {
        return "pre";
	}

	@Override
	public int filterOrder() {
		// TODO Auto-generated method stub
        return 1;
	}

}
